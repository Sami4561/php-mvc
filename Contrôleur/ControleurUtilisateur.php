<?php

class ControleurUtilisateur {
	function __construct() {
		global $rep,$vues,$dsn,$login,$mdp;
		session_start();
		$dVueErreur = array(); //tableau d'erreurs
		try{
			$action=$_REQUEST['action'];
			switch($action) {
				case NULL:
				$this->afficherListesPubliques();
				break;

				case "AjouterListePublicUser":
				$this->ajouterListePublicUser();
				break;

				case "AjouterListePrivateUser":
				$this->ajouterListePrivateUser();
				break;

				case "SupprimerListeUser":
				$this->supprimerListeUser();
				break;

				case "SupprimerListeUserModif":
				$this->supprimerListeUserModif();
				break;

				case "PageUtilisateur":
				$this->pageUtilisateur();
				break;

				case "Deconnexion":
				$this->deconnexion();
				break;

				default:
				$dVueErreur[] =	"Erreur d'appel php";
				require ($rep.$vues['erreur']);
				break;
			}

		} catch (PDOException $e)
		{
		//si erreur BD, pas le cas ici
			echo "Erreur : " . $e->getMessage();
		}
		catch (Exception $e2)
		{
			echo "Erreur : " . $e2->getMessage();
		}
		exit(0);
	}

	function ajouterListePublicUser(){
		global $rep, $vues;
		$nomListe = $_REQUEST['nomListe'];
		if(Validation::val_string($nomListe)){
			$liste = ModeleListe::ajouterListe($nomListe);
			$this->pageUtilisateur();
		} else {
			$dVueErreur[] = "Pas de balises dans les champs !";
			require ($rep.$vues['erreur']);
		}
	}

	function ajouterListePrivateUser(){
		global $rep, $vues;
		$nomListe = $_REQUEST['nomListe'];
		if(Validation::val_string($nomListe)){
			$liste = ModeleListe::ajouterListePrivate($nomListe);
			$this->pageUtilisateur();
		} else {
			$dVueErreur[] = "Pas de balises dans les champs !";
			require ($rep.$vues['erreur']);
		}
	}

	function supprimerListeUser(){
		global $rep, $vues;
		$idListe = $_REQUEST['idListe'];
		$liste = ModeleListe::supprimerListeByID($idListe);
		$this->pageUtilisateur();
	}

	function supprimerListeUserModif(){
		global $rep, $vues;
		$idListe = $_REQUEST['idListe'];
		$liste = ModeleListe::supprimerListeByID($idListe);
		$this->pageUtilisateur();
	}

	function pageUtilisateur(){
		global $rep, $vues;
		$tabL = ModeleListe::findAllListePublic();
		$username = $_SESSION['username'];
		$tabP = ModeleListe::findAllListePrivateByUsername($username);
		if(is_array($tabL)){
			foreach ($tabL as $liste) {
				$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
			}
		}
		if(is_array($tabP)){
			foreach ($tabP as $liste) {
				$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
			}
		}
		require ($rep.$vues['pagedutilisateur']);
	}

	function deconnexion(){
		global $rep, $vues;
		ModeleUtilisateur::deconnexion();
		$this->afficherListesPubliques();
	}

	function afficherListesPubliques(){
		global $rep, $vues;
		$tabL = ModeleListe::findAllListePublic();
		if(is_array($tabL)){
			foreach ($tabL as $liste) {
				$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
			}
		}
		require($rep.$vues['pagedaccueil']);
	}

	static function alerte($message) {
		echo "<script type='text/javascript'>alert('$message');</script>";
	}

	// static function confirmation() {
	// 	echo "<td><a onClick=\"javascript: return confirm('Veuillez confirmer la suppression.');\" href='delete.php?id=".$query2['id']."'>x</a></td><tr>";
	// }
}
