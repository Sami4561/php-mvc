<?php

class ControleurVisiteur {
	function __construct() {
		global $rep,$vues,$dsn,$login,$mdp;
		session_start();
		$dVueErreur = array(); //tableau d'erreurs
		try{
			$action=$_REQUEST['action'];
			switch($action) {
				case NULL:
				$this->afficherListesPubliques();
				break;

				case "AjouterListe":
				$this->ajouterListe();
				break;

				case "ModifierListe":
				$this->modifierListe();
				break;

				case "AjouterTache":
				$this->ajouterTache();
				break;

				case "CheckTache":
				$this->checkTache();
				break;

				case "UncheckTache":
				$this->uncheckTache();
				break;

				case "SupprimerListe":
				$this->supprimerListe();
				break;

				case "SupprimerTache":
				$this->supprimerTache();
				break;

				case "SupprimerTacheModif":
				$this->supprimerTacheModif();
				break;

				case "PageConnexion":
				$this->pageConnexion();
				break;

				case "Connexion":
				$this->connexion();
				break;

				case "Inscription":
				$this->inscription();
				break;

				case "InscriptionSuite":
				$this->inscriptionSuite();
				break;

				case "RetourMenu":
				$this->retourMenu();
				break;

				default:
				$dVueErreur[] =	"Erreur d'appel php";
				require ($rep.$vues['erreur']);
				break;
			}

		} catch (PDOException $e)
		{
		//si erreur BD, pas le cas ici
			echo "Erreur : " . $e->getMessage();

		}
		catch (Exception $e2)
		{
			echo "Erreur : " . $e2->getMessage();
		}
		exit(0);
	}

	function afficherListesPubliques(){
		global $rep, $vues;
		$tabL = ModeleListe::findAllListePublic();
		if(is_array($tabL)){
			foreach ($tabL as $liste) {
				$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
			}
		}
		require($rep.$vues['pagedaccueil']);
	}

	function ajouterListe(){
		global $rep, $vues, $dVueErreur;
		$nomListe = $_REQUEST['nomListe'];
		// $valid = Validation::val_string($nomListe);
		if(Validation::val_string($nomListe)){
			$liste = ModeleListe::ajouterListe($nomListe);
			$this->afficherListesPubliques();
		} else {
			$dVueErreur[] = "Pas de balises dans les champs !";
			require ($rep.$vues['erreur']);
		}
	}

	function ajouterTache(){
		global $rep, $vues;
		$nomTache = $_REQUEST['nomTache'];
		$idListe = $_REQUEST['idListe'];
		if(Validation::val_string($nomTache)){
			$tache = ModeleTache::ajouterTache($nomTache, $idListe);
			$this->modifierListe();
		}else{
			$dVueErreur[] = "Pas de balises dans le nom !";
			require($rep.$vues['erreur']);
		}
	}

	function modifierListe(){
		global $rep, $vues;
		$idListe = $_REQUEST['idListe'];
		$liste = ModeleListe::findListePublicByID($idListe);
		$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
		require ($rep.$vues['PageDeModificationListe']);
	}

	function modifierListeParam($idliste){
		global $rep, $vues;
		$liste = ModeleListe::findListePublicByID($idliste);
		$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
		require ($rep.$vues['PageDeModificationListe']);
	}

	function checkTache(){
		global $rep, $vues;
		$idTache = $_REQUEST['idTache'];
		$liste = ModeleListe::findListeByIDTache($idTache);
		$idliste = $liste->getIDListe();
		$tache = ModeleTache::checkTacheByID($idTache);
		$this->modifierListeParam($idliste);
	}

	function uncheckTache(){
		global $rep, $vues;
		$idTache = $_REQUEST['idTache'];
		$liste = ModeleListe::findListeByIDTache($idTache);
		$idliste = $liste->getIDListe();
		$tache = ModeleTache::uncheckTacheByID($idTache);
		$this->modifierListeParam($idliste);
	}

	function supprimerListe(){
		global $rep, $vues;
		$idListe = $_REQUEST['idListe'];
		$liste = ModeleListe::supprimerListeByID($idListe);
		$this->afficherListesPubliques();
	}

	function supprimerTache(){
		global $rep, $vues;
		$idTache = $_REQUEST['idTache'];
		$tache = ModeleTache::supprimerTacheByID($idTache);
		if(ModeleUtilisateur::isUser() == true) {
			$this->pageUtilisateur($username);
		}
		else
			$this->afficherListesPubliques();
	}

	function supprimerTacheModif(){
		global $rep, $vues;
		$idTache = $_REQUEST['idTache'];
		$liste = ModeleListe::findListeByIDTache($idTache);
		$idliste = $liste->getIDListe();
		$tache = ModeleTache::supprimerTacheByID($idTache);
		$this->modifierListeParam($idliste);
	}

	function pageConnexion(){
		global $rep, $vues;
		require ($rep.$vues['pagedeconnexion']);
	}

	function pageUtilisateur(){
		global $rep, $vues;
		$tabL = ModeleListe::findAllListePublic();
		$username = $_SESSION['username'];
		$tabP = ModeleListe::findAllListePrivateByUsername($username);
		if(is_array($tabL)){
			foreach ($tabL as $liste) {
				$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
			}
		}
		if(is_array($tabP)){
			foreach ($tabP as $liste) {
				$liste->setListeTache(ModeleTache::findAllTache($liste->getIDListe()));
			}
		}
		require ($rep.$vues['pagedutilisateur']);
	}

	function connexion(){
		global $rep, $vues;
		$username = $_REQUEST['username'];
		$pwd = $_REQUEST['pwd'];
		if(Validation::val_string($username, $pwd)){
			$user = ModeleUtilisateur::testConnexion($username, $pwd);
			if($user){
				$this->pageUtilisateur($username);
			} else {
				$this->alerte("Nom d\'utilisateur ou mot de passe incorrect !");
				$this->pageConnexion();
			}
		} else {
			$dVueErreur[] = "Pas de balises dans les champs !";
			require($rep.$vues['erreur']);
		}
	}

	function inscription(){
		global $rep, $vues;
		require ($rep.$vues['pagedinscription']);
	}

	//confirmation de l'inscription
	function inscriptionSuite(){
		global $rep, $vues;
		$username = $_REQUEST['username'];
		$pwd = $_REQUEST['pwd'];
		$cpwd = $_REQUEST['cpwd'];
		if(!(Validation::val_string($username, $pwd, $cpwd))){
			$dVueErreur[] = "Pas de balises dans les champs !";
			require ($rep.$vues['erreur']);
		} else {
			if ($pwd === $cpwd) { //vérification confirmation du mdp
				$user = ModeleUtilisateur::findUserByName($username); // on vérifie si le nom d'utilisateur n'est pas déjà pris
				$pwd = password_hash($pwd, PASSWORD_DEFAULT); //hachage du mot de passe
				// nom pas trouvé dans la BDD donc création
				if (is_null($user->getUsername())){ 
					$user = ModeleUtilisateur::addUser($username, $pwd);
					$this->afficherListesPubliques();
					$this->alerte('Inscription réussie, vous pouvez désormais vous connecter.');
				} else {
					$this->inscription();
					$this->alerte("Ce nom d\'utilisateur est déjà pris ! Veuillez en choisir un autre.");
				}
			} else {
				$dVueErreur[] = "Veuillez spécifier le même mot de passe !";
				require($rep.$vues['erreur']);
			} 
		}
	}

	function retourMenu(){
		global $rep, $vues;
		if(ModeleUtilisateur::isUser() == true) {
			$this->pageUtilisateur();
		}
		else {
			require($rep.$vues['pagedaccueil']);
		}
	}

	static function alerte($message) {
		echo "<script type='text/javascript'>alert('$message');</script>";
	}
}
