<?php

class FrontControleur{
	public function __construct(){
		global $rep, $vues;
		
		try{
			session_start();
			$listeAction_User = array('AjouterListePublicUser', 'AjouterListePrivateUser', 'SupprimerListeUser', 'SupprimerListeUserModif', 'Deconnexion', 'PageUtilisateur');
			$listeAction_Visiteur = array('AjouterListe', 'ModifierListe', 'AjouterTache', 'CheckTache', 'UncheckTache', 'SupprimerListe', 'SupprimerTache', 'SupprimerTacheModif', 'PageConnexion', 'Connexion', 'Inscription','InscriptionSuite', 'RetourMenu');
			$user = ModeleUtilisateur::isUser();

			if (isset($_GET["action"])) {
	                $action = $_REQUEST["action"];
	        } else { $action = null; }

			if(in_array($action, $listeAction_User)){
				if(!($user)){
					require($rep . $vues['pagedeconnexion']);
				} else { $ctrl = new ControleurUtilisateur(); }							
			} else $ctrl = new ControleurVisiteur();	
		} catch (Exception $e){
			require($rep.$vues['erreur']);
		}
	}
}
	


	

	
