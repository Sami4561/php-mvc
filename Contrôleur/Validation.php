<?php

class Validation {

	static function val_action($action) {

		if (!isset($action)) { throw new Exception('pas d\'action');}

	}

	static function val_string():bool{
		$args = func_get_args(); //on récupère les champs à filtrer
		foreach ($args as $champ) {
			if ($champ != filter_var($champ, FILTER_SANITIZE_STRING)){
       			return false; //Un des champs contenait des balises
			}
		}
		return true; //Tous les champs sont aptes à être utilisés
	}

	static function sanitize($champ){
		$champ = filter_var($champ, FILTER_SANITIZE_STRING);
		return $champ;
	}
}
?>
