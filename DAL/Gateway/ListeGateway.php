<?php


class ListeGateway
{
    private $con;

    public function __construct($c){
        $this->con=$c;
    }

    public function findAllListePublic():array{
        $query = "SELECT * FROM liste WHERE Username IS NULL ORDER BY IDListe";
        $this->con->executeQuery($query, array());
        return $this->con->getResults();
    }

    public function findAllListePrivateByUsername($username):array{
        $query = "SELECT * FROM liste WHERE Username =:username ORDER BY IDListe";
        $this->con->executeQuery($query, array(":username"=>array($username, PDO::PARAM_STR)));
        return $this->con->getResults();
    }

    public function findListePublicByID($idliste){
        $query = "SELECT * FROM liste WHERE IDListe =:idliste";
        $this->con->executeQuery($query, array(":idliste"=>array($idliste, PDO::PARAM_STR)));
        return $this->con->getResult();
    }

    public function findListeByIDTache($idtache){
        $query = "SELECT IDListe FROM Tache WHERE IDTache =:idtache";
        $this->con->executeQuery($query, array(":idtache"=>array($idtache, PDO::PARAM_STR)));
        return $this->con->getResult();
    }

    public function supprimerListeByID($idliste){
        $query = "DELETE FROM Liste WHERE IDListe =:idliste";
        $this->con->executeQuery($query, array(":idliste"=>array($idliste, PDO::PARAM_STR)));
    }

    public function ajouterListe($nom){
        $query = "INSERT INTO `liste` (`Nom`, `Username`) VALUES (:nom, NULL);";
        $this->con->executeQuery($query, array(":nom"=>array($nom, PDO::PARAM_STR)));
    }

    public function ajouterListePrivate($nom, $username){
        $query = "INSERT INTO `liste` (`Nom`, `Username`) VALUES (:nom, :username);";
        $this->con->executeQuery($query, array(":nom"=>array($nom, PDO::PARAM_STR), ":username"=>array($username, PDO::PARAM_STR)));
    }
}
