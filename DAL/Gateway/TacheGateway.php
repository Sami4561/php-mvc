<?php


class TacheGateway
{
    private $con;
    private $tabN;
    private $tabB;

    public function __construct($c){
        $this->con=$c;
    }

    public function findAllTache($idliste):array{
        $query = 'SELECT * FROM Tache WHERE IDListe =:idliste';
        $this->con->executeQuery($query, array(":idliste"=>array($idliste, PDO::PARAM_STR)));
        return $this->con->getResults();
    }

    public function findTacheByID($idtache){
        $query = "SELECT * FROM Tache WHERE IDTache =:idtache";
        $this->con->executeQuery($query, array(":idtache"=>array($idtache, PDO::PARAM_STR)));
        return $this->con->getResult();
    }

    public function checkTacheByID($idtache){
        $query = "UPDATE Tache SET Faite = '1' WHERE IDTache =:idtache";
        $this->con->executeQuery($query, array(":idtache"=>array($idtache, PDO::PARAM_STR)));
    }

    public function uncheckTacheByID($idtache){
        $query = "UPDATE Tache SET Faite = '0' WHERE IDTache =:idtache";
        $this->con->executeQuery($query, array(":idtache"=>array($idtache, PDO::PARAM_STR)));
    }

    public function deleteTache($idTache){
        $query = 'DELETE FROM Tache WHERE IDTache =:idTache';
        $this->con->executeQuery($query, array(":idTache"=>array($idTache, PDO::PARAM_STR)));
    }

    public function supprimerTacheByID($idtache){
        $query = "DELETE FROM Tache WHERE IDTache =:idtache";
        $this->con->executeQuery($query, array(":idtache"=>array($idtache, PDO::PARAM_STR)));
    }

    public function ajouterTache($nom, $idListe){
        $query = "INSERT INTO `tache` (`Description`, `Faite`, `IDListe`) VALUES (:nom, 0, :idListe);";
        $this->con->executeQuery($query, array(":nom"=>array($nom, PDO::PARAM_STR), ":idListe"=>array($idListe, PDO::PARAM_STR)));
    }
}
