<?php


class UtilisateurGateway
{
    private $con;

    public function __construct($c){
        $this->con=$c;
    }

    public function testConnexion($username, $pwd){
        $query = "SELECT * FROM utilisateur WHERE Username =:username";
        $this->con->executeQuery($query, array(":username"=>array($username, PDO::PARAM_STR)));
        $result = $this->con->getResults();
        return password_verify($pwd, $result[0]['Pwd']); //comparaison mdp entré par l'utilisateur et valeur hachée dans la BDD 
    }

    public function findUserByName($username){
    	$query = "SELECT * FROM Utilisateur WHERE Username =:username";
        $this->con->executeQuery($query, array(":username"=>array($username, PDO::PARAM_STR)));
        return $this->con->getResult();
    }

    public function addUser($username, $pwd){
    	$query = "INSERT INTO `utilisateur` (`Username`, `Pwd`) VALUES (:username, :pwd);";
        $this->con->executeQuery($query, array(":username"=>array($username, PDO::PARAM_STR),":pwd"=>array($pwd, PDO::PARAM_STR)));
    }
}
