<?php

class ModeleListe {
	
	public static function findAllListePublic(){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findAllListePublic();
		foreach ($result as $row) {
			$tabN[] = new Liste($row['IDListe'], $row['Nom'], $row['Username']);
		}
		return $tabN;
	}

	public static function findAllListePrivateByUsername($username){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findAllListePrivateByUsername($username);
		foreach ($result as $row) {
			$tabN[] = new Liste($row['IDListe'], $row['Nom'], $row['Username']);
		}
		return $tabN;
	}

	public static function findListePublicByID($idliste){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findListePublicByID($idliste);
		return new Liste($result['IDListe'], $result['Nom'], $result['Username']);
	}

	public static function findListeByIDTache($idtache){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findListeByIDTache($idtache);
		return new Liste($result['IDListe'], $result['Nom'], $result['Username']);
	}

	public static function supprimerListeByID($idliste){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->supprimerListeByID($idliste);
		return $result;
	}

	public static function ajouterListe($nom){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->ajouterListe($nom);
		return $result;
	}

	public static function ajouterListePrivate($nom){
		global $dsn, $login, $mdp;
		$gateway = new ListeGateway(new Connexion($dsn, $login, $mdp));
		$username = $_SESSION['username'];
		$result = $gateway->ajouterListePrivate($nom, $username);
		return $result;
	}
}
