<?php

class ModeleTache {
	
	public static function findAllTache($idliste){
		global $dsn, $login, $mdp;
		$gateway = new TacheGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findAllTache($idliste);
		foreach ($result as $row) {
			$tabN[] = new Tache($row['IDTache'], $row['Description'], $row['Faite'], $row['IDListe']);
		}
		return $tabN;
	}

	public static function findTacheByID($idtache){
		global $dsn, $login, $mdp;
		$gateway = new TacheGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findTacheByID($idtache);
		return new Tache($result['IDTache'], $result['Description'], $result['Faite'], $result['IDListe']);
	}

	public static function checkTacheByID($idtache){
		global $dsn, $login, $mdp;
		$gateway = new TacheGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->checkTacheByID($idtache);
		return new Tache($result['IDTache'], $result['Description'], $result['Faite'], $result['IDListe']);
	}

	public static function uncheckTacheByID($idtache){
		global $dsn, $login, $mdp;
		$gateway = new TacheGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->uncheckTacheByID($idtache);
		return new Tache($result['IDTache'], $result['Description'], $result['Faite'], $result['IDListe']);
	}

	public static function supprimerTacheByID($idtache){
		global $dsn, $login, $mdp;
		$gateway = new TacheGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->supprimerTacheByID($idtache);
		return $result;
	}

	public static function ajouterTache($nom, $idListe){
		global $dsn, $login, $mdp;
		$gateway = new TacheGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->ajouterTache($nom, $idListe);
		return $result;
	}
}
