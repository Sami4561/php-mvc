<?php

class ModeleUtilisateur {
	public static function testConnexion($username, $pwd):bool{
        global $dsn, $login, $mdp;
        $gateway = new UtilisateurGateway(new Connexion($dsn, $login, $mdp));
        if (Validation::val_string($username, $pwd)){
            $result = $gateway->testConnexion($username, $pwd);
            if($result){
                $_SESSION['classe']='user';
                $_SESSION['username']=$username;
                return true;
            }
            else return false;
        } 
        return false; //balises
    }

	public static function deconnexion(){
		session_unset();
		session_destroy();
		$_SESSION = array();
	}

	public static function isUser(){
        if (isset($_SESSION['username']) && isset($_SESSION['classe'])) {
            $username = Validation::sanitize($_SESSION['username']);
            $classe = Validation::sanitize($_SESSION['classe']);
            return new Utilisateur($username, $classe);
        }
        else return null;
	}

	public static function findUserByName($username){
		global $dsn, $login, $mdp;
		$gateway = new UtilisateurGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->findUserByName($username);
		return new Utilisateur($result['Username'], $result['Pwd']);
    }

    public static function addUser($username, $pwd){
    	global $dsn, $login, $mdp;
		$gateway = new UtilisateurGateway(new Connexion($dsn, $login, $mdp));
		$result = $gateway->addUser($username, $pwd);
		return new Utilisateur($result['Username'], $result['Pwd']);
    }
}
