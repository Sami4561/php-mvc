<?php


class Liste
{
    private $IDListe;
    private $Nom;
    private $Username;
    private $ListeTache = array();

    public function __construct($IDListe, $Nom, $Username){
        $this->setIDListe($IDListe);
        $this->setNom($Nom);
        $this->setUsername($Username);
    } 


    public function getIDListe()
    {
        return $this->IDListe;
    }

    /**
     * @param mixed $IDListe
     */
    public function setIDListe($IDListe)
    {
        $this->IDListe = $IDListe;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->Nom;
    }

    /**
     * @param mixed $Nom
     */
    public function setNom($Nom)
    {
        $this->Nom = $Nom;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * @param mixed $Username
     */
    public function setUsername($Username)
    {
        $this->Username = $Username;
    }

    public function getListeTache()
    {
        return $this->ListeTache;
    }

    public function setListeTache($listetache)
    {
        $this->ListeTache = $listetache;
    }
}
