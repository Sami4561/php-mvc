<?php

class Tache
{
    private $IDTache;
    private $Description;
    private $Faite;
    private $IDListe;

    public function __construct($IDTache, $Description, $Faite, $IDListe) {
        $this -> setIDTache($IDTache);
        $this -> setDescription($Description);
        $this -> setFaite($Faite);
        $this -> setIDListe($IDListe);
    }

    /**
     * @param mixed $IDListe
     */
    public function setIDListe($IDListe)
    {
        $this->IDListe = $IDListe;
    }

    /**
     * @return mixed
     */
    public function getIDListe()
    {
        return $this->IDListe;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @return mixed
     */
    public function getFaite()
    {
        return $this->Faite;
    }

    /**
     * @return mixed
     */
    public function getIDTache()
    {
        return $this->IDTache;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @param mixed $Faite
     */
    public function setFaite($Faite)
    {
        $this->Faite = $Faite;
    }

    /**
     * @param mixed $IDTache
     */
    public function setIDTache($IDTache)
    {
        $this->IDTache = $IDTache;
    }
}