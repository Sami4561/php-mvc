<?php

class Utilisateur
{
    private $Username;
    private $Pwd;

    public function __construct($Username, $Pwd) {
        $this -> setUsername($Username);
        $this -> setPwd($Pwd);
    }

    /**
     * @param mixed $Username
     */
    public function setUsername($Username)
    {
        $this->Username = $Username;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * @return mixed
     */
    public function getPwd()
    {
        return $this->Pwd;
    }

    /**
     * @param mixed $Pwd
     */
    public function setPwd($Pwd)
    {
        $this->Pwd = $Pwd;
    }
}