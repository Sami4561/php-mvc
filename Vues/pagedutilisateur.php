<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="Vues/CSS/pagedutilisateur.css">
	<title>ToDoList</title>
</head>

<body>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light" id="barre_nav">
		<div id="titre">
			<img id="icone_site" src="Vues/Ressources/icone_site.png" width="40" height="40">
			<a class="navbar-brand text-white"  href="index.php">To Do List</a>
		</div>
		<button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<?php 
				if(ModeleUtilisateur::isUser() == null) { ?>
					<li class="nav-item active">
						<a class="nav-link text-white" href="index.php?action=PageConnexion">Connexion</a>
					</li>
				<?php } else { ?>
					<li class="nav-item">
						<a class="nav-link text-white" href="index.php?action=PageUtilisateur" >Mes listes</a>
					</li>
					<li class="nav-item active">
						<a class="nav-link text-white" href="index.php?action=Deconnexion">Déconnexion</a>
					</li>
				<?php } ?> 
				<li class="nav-item">
					<a class="nav-link text-white" href="index.php?action=Inscription">Inscription</a>
				</li>
			</ul>
			<?php 
			if(ModeleUtilisateur::isUser() ==! null) {
				?>
				<ul class="nav navbar-nav flex-row justify-content-md-center justify-content-start flex-nowrap">
					<li class="li_etat">
						<a class="text-white">Connecté en tant que <?php echo $_SESSION['username']; ?></a>
					</li>
				</ul>
			<?php } else { ?>
				<ul class="nav navbar-nav flex-row justify-content-md-center justify-content-start flex-nowrap">
					<li class="li_etat">
						<a class="text-white">Non connecté</a>
					</li>
				</ul>
			<?php } ?>
		</div>
	</nav>

	<div align="center">
		<?php
		if (isset($dataVueEreur) && count($dataVueEreur)>0) {
			echo "<h2>ERREUR !!!!!</h2>";
			foreach ($dataVueEreur as $value){
				echo $value;
			}}
			?>


			<h1 class="titre">
				<img src="Vues/Ressources/icone_site.png">
				ToDoList
			</h1>

		</div>

		<div class="row">
			<div class="col-1"></div>
			<div class="col-4">
				<div class="main-section">
					<div class="add-section">
						<form method="POST" action="index.php?action=AjouterListePublicUser">
							<input type="text" name="nomListe" class="liste_input" placeholder="Insérez un nom de liste" required>
							<button type="submit" class="tache_btn" name="submit">Ajouter liste publique</button>
						</form>
					</div>
					<div class="show-todo-section">
						<h1 class="text-dark text-center">Listes publiques :</h1>
						<br>
						<?php
						if($tabL ==! null){
							foreach ($tabL as $liste) { ?>
								<h2 class="text-dark"><?php echo $liste->getNom() ?></h2>
								<form method="POST" action="index.php?action=ModifierListe">
									<input type="hidden" name="idListe" value="<?php echo $liste->getIDListe(); ?>">
									<button type="submit" class="btn btn-outline-primary btn-modif-liste">Modifier la liste</button>
								</form>
								<form method="POST" action="index.php?action=SupprimerListeUser">
									<input type="hidden" name="idListe" value="<?php echo $liste->getIDListe(); ?>">
									<button class="btn btn-danger btn-suppr-liste" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette liste ?');">X</button>
								</form>
								<?php
								if($liste->getListeTache()==!null){
									foreach ($liste->getListeTache() as $tache) {
										?>
										<div class="todo-item">
											<?php if($tache->getFaite() == 1){ ?>
												<form method="POST" action="index.php?action=SupprimerTache">
													<input type="hidden" name="idTache" value="<?php echo $tache->getIDTache(); ?>">
													<button class="btn btn-outline-danger btn-suppr-tache">X</button>
												</form>
												<h2 class="checked"><?php echo $tache->getDescription() ?></h2>
											<?php }else { ?>
												<form method="POST" action="index.php?action=SupprimerTache">
													<input type="hidden" name="idTache" value="<?php echo $tache->getIDTache(); ?>">
													<button class="btn btn-outline-danger btn-suppr-tache">X</button>
												</form>
												<h2><?php echo $tache->getDescription() ?></h2>
											<?php } ?>
										</div>
									<?php } ?>
									<br>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="col-2"></div>
			<div class="col-4">
				<div class="main-section">
					<div class="add-section">
						<form method="POST" action="index.php?action=AjouterListePrivateUser">
							<input type="text" name="nomListe" class="liste_input" placeholder="Insérez un nom de liste" required>
							<button type="submit" class="tache_btn" name="submit">Ajouter liste privée</button>
						</form>
					</div>
					<div class="show-todo-section">
						<h1 class="text-dark text-center">Listes privées :</h1>
						<br>
						<?php
						if(is_array($tabP)){
							foreach ($tabP as $liste) { ?>
								<h2 class="text-dark"><?php echo $liste->getNom() ?></h2>
								<form method="POST" action="index.php?action=ModifierListe">
									<input type="hidden" name="idListe" value="<?php echo $liste->getIDListe(); ?>">
									<button type="submit" class="btn btn-outline-primary btn-modif-liste">Modifier la liste</button>
								</form>
								<form method="POST" action="index.php?action=SupprimerListeUser">
									<input type="hidden" name="idListe" value="<?php echo $liste->getIDListe(); ?>">
									<button class="btn btn-danger btn-suppr-liste" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette liste ?');">X</button>
								</form>
								<?php
								if($liste->getListeTache()==!null){
									foreach ($liste->getListeTache() as $tache) {
										?>
										<div class="todo-item">
											<?php if($tache->getFaite() == 1){ ?>
												<form method="POST" action="index.php?action=SupprimerTache">
													<input type="hidden" name="idTache" value="<?php echo $tache->getIDTache(); ?>">
													<button class="btn btn-outline-danger btn-suppr-tache">X</button>
												</form>
												<h2 class="checked"><?php echo $tache->getDescription() ?></h2>
											<?php }else { ?>
												<form method="POST" action="index.php?action=SupprimerTache">
													<input type="hidden" name="idTache" value="<?php echo $tache->getIDTache(); ?>">
													<button class="btn btn-outline-danger btn-suppr-tache">X</button>
												</form>
												<h2><?php echo $tache->getDescription() ?></h2>
											<?php } ?>
										</div>
									<?php } ?>
									<br>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-1"></div>
	</div>




	<!--  -->


	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</body>
</html>
