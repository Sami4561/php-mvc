-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 04 jan. 2021 à 18:44
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `todolist`
--

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

DROP TABLE IF EXISTS `liste`;
CREATE TABLE IF NOT EXISTS `liste` (
  `IDListe` int(4) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Username` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`IDListe`),
  KEY `fk_user` (`Username`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `liste`
--

INSERT INTO `liste` (`IDListe`, `Nom`, `Username`) VALUES
(1, 'Maison', NULL),
(2, 'Travail', NULL),
(59, 'Test', 'Test'),
(62, 'Test', 'Tux');

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

DROP TABLE IF EXISTS `tache`;
CREATE TABLE IF NOT EXISTS `tache` (
  `IDTache` int(4) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) NOT NULL,
  `Faite` tinyint(1) DEFAULT NULL,
  `IDListe` char(4) DEFAULT NULL,
  PRIMARY KEY (`IDTache`),
  KEY `fk_liste` (`IDListe`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tache`
--

INSERT INTO `tache` (`IDTache`, `Description`, `Faite`, `IDListe`) VALUES
(15, 'Test', 0, '59'),
(3, 'Faire le TP de PHP', 0, '2'),
(14, 'Tondre la pelouse', 1, '1');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `Username` varchar(20) NOT NULL,
  `Pwd` varchar(100) NOT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`Username`, `Pwd`) VALUES
('Test', '$2y$10$D.ePwvpg.q2uflWx9u.DR.Vp/M4eO74Q60vDoqPXsx9c.UFcBEDyG'),
('Tux', '$2y$10$bFWOPq/dvaD50BfcpUO3I.qortxDEuG4RGuEBJjHBfotOzF8PaT8m');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
